/*hw
* 2)Дано целое число. Если число чётное, то к нему прибавить 2, а если нечётное, то 1. Не использовать условие "если" (if).*/

public class Main {

    public static void main (String[]args){
        int i = 9;
        switch (i % 2) {
            case 0:
                i +=2;
                System.out.print(i);
                break;
            case 1:
                i+=1;
                System.out.print(i);
                break;
        }
    }
}